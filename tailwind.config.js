module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./src/**/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        lime: "#6dee6d",
        peach: "#ff9f5f",
      },
    },
  },
  plugins: [],
};
