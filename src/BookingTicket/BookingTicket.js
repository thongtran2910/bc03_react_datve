import React, { Component } from "react";
import BookingLayout from "./BookingLayout";

export default class BookingTicket extends Component {
  render() {
    return (
      <div
        className="h-screen w-screen bookingMovie bg-no-repeat bg-cover bg-center"
        style={{ backgroundImage: "url(./img/bgmovie.jpg)" }}
      >
        <BookingLayout />
      </div>
    );
  }
}
