import { dataTicket } from "../../dataTicket";
import { ADD_TICKET, DEL_TICKET } from "../constaints/constant";

let initialState = {
  ticketList: dataTicket,
  dsVeDaDat: [],
};
export const bookingTicketReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case ADD_TICKET: {
      let cloneVeDaDat = [...state.dsVeDaDat];
      let index = cloneVeDaDat.findIndex((item) => {
        return item.soGhe == payload.soGhe;
      });
      if (index == -1) {
        let newTicket = { ...payload };
        cloneVeDaDat.push(newTicket);
      } else {
        cloneVeDaDat.splice(index, 1);
      }
      state.dsVeDaDat = cloneVeDaDat;
      return { ...state };
    }
    case DEL_TICKET: {
      let cloneVeDaDat = [...state.dsVeDaDat];
      let index = cloneVeDaDat.findIndex((item) => {
        return item.soGhe == payload;
      });
      if (index !== -1) {
        cloneVeDaDat.splice(index, 1);
        state.dsVeDaDat = cloneVeDaDat;
        return { ...state };
      }
    }
    default:
      return state;
  }
};
