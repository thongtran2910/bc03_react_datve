import React, { Component } from "react";
import { connect } from "react-redux";
import { DEL_TICKET } from "./redux/constaints/constant";

class Cart extends Component {
  render() {
    return (
      <div>
        <table className="table-fixed w-full text-xl text-black mt-5 p-5">
          <thead className="border-2 border-orange-500  bg-peach">
            <tr>
              <td>Số ghế</td>
              <td>Giá</td>
              <td></td>
            </tr>
          </thead>
          <tbody className="bg-teal-100 border-orange-500 border-2">
            {this.props.dsVeDaDat.map((item) => {
              return (
                <tr>
                  <td>{item.soGhe}</td>
                  <td>{item.gia}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaVe(item.soGhe);
                      }}
                      className="text-red-500"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                        />
                      </svg>
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="text-left text-2xl bg-teal-100 border-orange-500 border-2 px-5 py-2">
          <p>TỔNG {this.props.dsVeDaDat.length} VÉ</p>
          <p>
            SỐ TIỀN:{" "}
            {this.props.dsVeDaDat.reduce(
              (total, currentValue) => (total = total + currentValue.gia),
              0,
              1
            )}{" "}
            VND
          </p>
        </div>
        <div className="text-right mt-5">
          <button className="bg-peach hover:bg-orange-500 text-black font-bold py-2 px-4 border border-orange-500 rounded">
            Xác nhận
          </button>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    dsVeDaDat: state.bookingTicketReducer.dsVeDaDat,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleXoaVe: (idTicket) => {
      dispatch({
        type: DEL_TICKET,
        payload: idTicket,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
