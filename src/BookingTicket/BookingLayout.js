import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import { ADD_TICKET, CHANGE_SEAT_STATUS } from "./redux/constaints/constant";

class BookingLayout extends Component {
  render() {
    return (
      <div className="container flex flex-row mx-auto">
        <div className="basis-3/4 mb-5">
          <h2 className="text-yellow-300 font-bold text-3xl mt-4">
            ĐẶT VÉ XEM PHIM
          </h2>
          <h3 className="text-gray-400 font-bold text-xl mt-5">MÀN HÌNH</h3>
          <div className="screen mb-5 mx-auto"></div>
          <div className="my-5 mr-3">
            {this.props.ticketList.map((item) => {
              const seatList = item.danhSachGhe;
              return (
                <div className="text-xl my-2 grid grid-rows-11 grid-flow-col">
                  <div className="firstChar w-[40px] h-[35px]">{item.hang}</div>
                  {seatList.map((data, index) => {
                    return (
                      <button
                        key={index}
                        onClick={() => {
                          this.props.handleDatVe(data);
                        }}
                        className="w-[40px] h-[35px] btn bg-slate-300 border-2 border-orange-500 rounded-lg"
                      >
                        {data.soGhe}
                      </button>
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
        <div className="basis-1/4">
          <h2 className="my-5 font-bold text-3xl text-white">
            DANH SÁCH GHẾ CỦA BẠN
          </h2>
          <div>
            <div className="flex flex-row items-end ">
              <div className="bg-orange-500 w-[40px] h-[35px] rounded-lg"></div>
              <p className="text-white">Ghế đã đặt</p>
            </div>
            <div className="flex flex-row items-end my-2">
              <div className="bg-lime w-[40px] h-[35px] rounded-lg"></div>
              <p className="text-white">Ghế đang chọn</p>
            </div>
            <div className="flex flex-row items-end">
              <div className="w-[40px] h-[35px] border-2 border-orange-500 bg-slate-300 rounded-lg"></div>
              <p className="text-white">Ghế chưa đặt</p>
            </div>
          </div>
          {this.props.dsVeDaDat.length > 0 && <Cart />}
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    ticketList: state.bookingTicketReducer.ticketList,
    dsVeDaDat: state.bookingTicketReducer.dsVeDaDat,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleDatVe: (ticket) => {
      dispatch({
        type: ADD_TICKET,
        payload: ticket,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingLayout);
